import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './core/auth/auth.component';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { UnauthorizedComponent } from './errors/unauthorized/unauthorized.component';
import { GlobalErrorComponent } from './errors/global-error/global-error.component';
import { AlterarSenhaComponent } from './core/alterar-senha/alterar-senha.component';
import { DadosCadastraisComponent } from './dados-cadastrais/dados-cadastrais.component';
import { DadosContatoComponent } from './dados-contato/dados-contato.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AuthComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: 'global-error', component: GlobalErrorComponent },
  { path: 'alterar-senha', component: AlterarSenhaComponent },
  { path: 'dados-contato', component: DadosContatoComponent },
  { path: 'dados-cadastrais', redirectTo: 'dados-cadastrais/dadospessoais' },
  { path: 'dados-cadastrais/:etapa', component: DadosCadastraisComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }