import { ElementRef, Renderer2 } from '@angular/core';

export function changeStyle(renderer: Renderer2, element: ElementRef, hora: any, direction = 'bottom') {
    switch (true) {
        case hora >= 6 && hora < 12:
            renderer.setStyle(element.nativeElement, 'background-image', `linear-gradient(to ${direction}, #f2f2f2, #40bf80)`);
            break;

        case hora >= 12 && hora < 18:
            renderer.setStyle(element.nativeElement, 'background-image', `linear-gradient(to ${direction}, #f2f2f2, #0099cc)`);
            break;

        case hora >= 18 && hora < 24:
            renderer.setStyle(element.nativeElement, 'background-image', `linear-gradient(to ${direction}, #f2f2f2, #2e2eb8 )`);
            break;

        default:
            renderer.setStyle(element.nativeElement, 'background-image', `linear-gradient(to ${direction}, #f2f2f2, #191966)`);
            break;
    }
}