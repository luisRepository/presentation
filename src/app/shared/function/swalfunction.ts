import swal, { SweetAlertIcon } from 'sweetalert2';

export function swalAlert(mensagem: string, icon?: SweetAlertIcon) {
    swal.fire(mensagem, '', icon);
}