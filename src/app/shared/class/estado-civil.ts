export enum EstadoCivil {
    Casado = 1,
    Desquitado = 2,
    Divorciado = 3,
    Solteiro = 5,
    Viuvo = 6
}