import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputComponent } from './input/input.component';
import { NgxLoadingXModule } from 'ngx-loading-x';

@NgModule({
  declarations: [InputComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingXModule
  ],
  exports: [
    InputComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingXModule
  ],
  providers: []
})
export class SharedModule { }