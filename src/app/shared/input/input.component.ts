import { Component, ContentChild, Input, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import { FormControlName, NgModel } from '@angular/forms';
import { VirtualTimeScheduler } from 'rxjs';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, AfterContentInit {
  @Input() label:string;
  @Input() erroMessage: string;

  input: any;

  @ContentChild(NgModel) model: NgModel
  @ContentChild(FormControlName) control: FormControlName;

  constructor() { }

  ngOnInit() {
  }

  hasSuccess():boolean{
    return this.input.valid && (this.input.dirty || this.input.touched);
  }

  hasError():boolean{
    return this.input.invalid && (this.input.dirty || this.input.touched);
  }

  ngAfterContentInit(){
    this.input = this.model || this.control;
    if(this.input === undefined){
      throw new Error("Esse Componente precisa ser usado com uma diretiva ngModel ou formControlName");
    }
  }

}