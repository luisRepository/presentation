import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { changeStyle } from '../shared/function/stylefunction';

@Component({
    selector: 'app-dados-cadastrais',
    templateUrl: './dados-cadastrais.component.html',
    styleUrls: ['./dados-cadastrais.component.scss']
})
export class DadosCadastraisComponent implements OnInit {

    @ViewChild('containerdados') element: ElementRef;
    horaStyle = new Date;
    etapa: Etapas;
    etapas = Etapas;

    constructor(private renderer: Renderer2, 
        private router: Router,
        private activatedRoute: ActivatedRoute,) { }

    ngOnInit(): void {
        this.activatedRoute.params.subscribe(params => {
            let etapa = Etapas[params.etapa.toLocaleLowerCase()];
            this.etapa = etapa as Etapas;
        });
    }

    ngAfterViewInit() {
        changeStyle(this.renderer, this.element, this.horaStyle.getHours(), 'top');
    }

    mudarEtapa(etapa: Etapas){

    }

}

export enum Etapas {
    dadospessoais = 'dadospessoais',
    beneficiarioshabilitados = 'beneficiarioshabilitados',
    dadoscontato = 'dadoscontato'
}