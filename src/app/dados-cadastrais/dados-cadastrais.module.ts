import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from './../shared/shared.module';
import { DadosCadastraisComponent } from './dados-cadastrais.component';
import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';

@NgModule({
    declarations: [
        DadosCadastraisComponent,
        DadosPessoaisComponent
    ],
    imports: [
        SharedModule,
        RouterModule,
    ],
    exports: [
        DadosCadastraisComponent,
        DadosPessoaisComponent
    ]
})
export class DadosCadastraisModule { }