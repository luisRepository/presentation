import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ErrorHandler, NgModule } from '@angular/core';
import { NotFoundComponent } from './not-found/not-found.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { GlobalErrorComponent } from './global-error/global-error.component';
import { GlobalErrorHandler } from './global-error-handler/global-error.handler';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [NotFoundComponent, UnauthorizedComponent, GlobalErrorComponent],
    providers: [{
        provide: ErrorHandler,
        useClass: GlobalErrorHandler
    }]
})
export class ErrorsModule { }