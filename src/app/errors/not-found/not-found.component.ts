import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { changeStyle } from '../../shared/function/stylefunction';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit, AfterViewInit {
  @ViewChild('button') element: ElementRef;
  horaStyle = new Date;
  
  ngOnInit() {}

  constructor(private renderer: Renderer2) { }

  voltar() {
    window.location.href = "";
  }

  ngAfterViewInit() {
    changeStyle(this.renderer, this.element, this.horaStyle.getHours());
  }

}