import { environment } from './../../../environments/environment';
import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import * as StackTrace from 'stacktrace-js';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    private router: Router;

    constructor(private injector: Injector,
        private zone: NgZone) {
        setTimeout(() => {
            this.router = injector.get(Router)
        })
    }

    handleError(error: any): void {

        if (error.status == 401) {
            this.zone.run(() => { this.router.navigate(['/unauthorized']); });
        }
        else if (error.status == 403) {
            //this.tokenService.removeToken();
            window.location.href = 'unauthorized';
        }
        else {
            console.log(error)
            this.zone.run(() => { this.router.navigate(['/not-found']); });
        }

        if (!environment.production && !error.url && !error.headers) {
            const location = this.injector.get(LocationStrategy);
            const url = location instanceof PathLocationStrategy ? location.path() : '';
            const message = error.message ? error.message : error.toString();

            StackTrace
                .fromError(error)
                .then(stackFrames => {
                    const stackAsString = stackFrames
                        .map(sf => sf.toString())
                        .join('\n');

                    console.log(url);
                    console.log(message);
                    console.log(stackAsString);
                });
        }
    }
}