import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { changeStyle } from '../../shared/function/stylefunction';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent implements OnInit, AfterViewInit {
  @ViewChild('button') element: ElementRef;
  horaStyle = new Date;

  ngOnInit() { }

  constructor(private renderer: Renderer2) { }

  voltar() {
    window.location.href = "";
  }

  ngAfterViewInit() {
    changeStyle(this.renderer, this.element, this.horaStyle.getHours());
  }

}