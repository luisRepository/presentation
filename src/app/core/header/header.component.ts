import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { changeStyle } from '../../shared/function/stylefunction';
import { AuthService } from './../services/auth.service';
import { TokenService } from './../token/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('header') element: ElementRef;
  @ViewChild('dropdown') elementDrop: ElementRef;
  horaStyle = new Date;
  public load = false;

  constructor(private renderer: Renderer2,
    private authService: AuthService,
    private tokenService: TokenService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    changeStyle(this.renderer, this.element, this.horaStyle.getHours());
  }

  changeStyleDropdown() {
    changeStyle(this.renderer, this.elementDrop, this.horaStyle.getHours());
  }

  excluirUsuario() {
  }

  deslogar() {
    this.authService.deslogar().subscribe(() => {
      this.tokenService.removeToken();
      window.location.href = "";
    }, error => {
      console.log(error);
    })
  }
}