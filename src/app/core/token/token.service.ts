import { Injectable } from '@angular/core';

const KEY = "Token";

@Injectable({
    providedIn: 'root'
})
export class TokenService {

    hasToken() {
        return !!this.getToken();
    }

    setToken(tokenId: any) {
        sessionStorage.setItem(KEY, tokenId);
    }

    getToken(): any {
        let tokenId = sessionStorage.getItem(KEY);

        if (tokenId) {
            return tokenId;
        }
        return null;
    }

    removeToken() {
        sessionStorage.removeItem(KEY);
    }

}