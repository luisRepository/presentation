export interface TokenModel {

    Id: string,
    userId: string,
    userLogin: number,
    expirationDate: Date,
}