import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { changeStyle } from '../../shared/function/stylefunction'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {
  ano: number;
  @ViewChild('footer') element: ElementRef
  horaStyle = new Date;

  constructor(private render: Renderer2) { }

  ngOnInit(): void {
    this.ano = new Date().getFullYear();
  }

  ngAfterViewInit() {
    changeStyle(this.render, this.element, this.horaStyle.getHours());
  }

}