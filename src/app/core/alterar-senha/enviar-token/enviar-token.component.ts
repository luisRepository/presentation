import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-enviar-token',
  templateUrl: './enviar-token.component.html',
  styleUrls: ['./enviar-token.component.scss']
})
export class EnviarTokenComponent implements OnInit {

  @Output() mudarEtapa = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  enviar(){
    this.mudarEtapa.emit();
  }

}
