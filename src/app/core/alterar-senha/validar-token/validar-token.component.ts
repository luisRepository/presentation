import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-validar-token',
  templateUrl: './validar-token.component.html',
  styleUrls: ['./validar-token.component.scss']
})
export class ValidarTokenComponent implements OnInit {

  tokenForm: FormGroup
  @Input() userLogin: string;
  @Output() mudarEtapa = new EventEmitter();

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm(): void {
    this.tokenForm = this.fb.group({
      token: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  get token(): FormControl {
    return <FormControl>this.tokenForm.get('token');
  }

  onSubmit() {
    this.mudarEtapa.emit();
  }

}