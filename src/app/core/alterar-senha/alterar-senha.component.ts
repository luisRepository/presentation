import { Component, ElementRef, OnInit, ViewChild, Renderer2, AfterViewInit } from '@angular/core';
import { changeStyle } from '../../shared/function/stylefunction';

@Component({
  selector: 'app-alterar-senha',
  templateUrl: './alterar-senha.component.html',
  styleUrls: ['./alterar-senha.component.scss']
})
export class AlterarSenhaComponent implements OnInit, AfterViewInit {

  @ViewChild('containerSenha') element: ElementRef;
  @ViewChild('containerMensagem') elementMensage: ElementRef;
  horaStyle = new Date;
  etapa: Etapas;
  userLogin: string;

  constructor(private render: Renderer2) { }

  ngOnInit(): void {
    this.etapa = Etapas.EnviarToken;
}

  ngAfterViewInit() {
    changeStyle(this.render, this.element, this.horaStyle.getHours(), 'top');
    changeStyle(this.render, this.elementMensage, this.horaStyle.getHours(), 'top');
  }

  validarToken(){
    this.etapa = Etapas.ValidarToken;
  }

  salvarSenha() {
    this.etapa = Etapas.SalvarSenha;
}

}

export enum Etapas {
  EnviarToken = 'EnviarToken',
  ValidarToken = 'ValidarToken',
  SalvarSenha = 'SalvarSenha'
}