import { PasswordRequest } from './../alterar-senha.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-salvar-senha',
    templateUrl: './salvar-senha.component.html',
    styleUrls: ['./salvar-senha.component.scss']
})
export class SalvarSenhaComponent implements OnInit {

    passwordForm: FormGroup;
    passwordRequest: PasswordRequest;

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.createForm();
    }

    private createForm(): void {
        this.passwordForm = this.fb.group({
            senha: ['', [Validators.required, Validators.minLength(6)]],
            repetirsenha: ['', Validators.required, Validators.minLength(6)]
        });
    }

    onSubmit(){
        
    }

}