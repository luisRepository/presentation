import { Component, ElementRef, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { changeStyle } from '../../shared/function/stylefunction';
import { swalAlert } from '../../shared/function/swalfunction';
import swal from 'sweetalert2';
import { AccountService } from '../services/account.service';
import { AuthService } from './../services/auth.service';
import { TokenService } from './../token/token.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
    authForm: FormGroup;
    configs = {
        isSignIn: true,
        action: 'Login',
        actionChange: 'Ainda não tem conta? Criar Conta!'
    };
    confirmaSenhaControl = new FormControl('', [Validators.required]);
    @ViewChild('containerlogin') element: ElementRef;
    horaStyle = new Date;
    load = false;


    constructor(private fb: FormBuilder,
        private render: Renderer2,
        private router: Router,
        private accountService: AccountService,
        private authService: AuthService,
        private tokenService: TokenService) { }

    ngOnInit() {
        this.createForm();
    }

    ngAfterViewInit() {
        changeStyle(this.render, this.element, this.horaStyle.getHours(), 'top');
    }

    private createForm(): void {
        this.authForm = this.fb.group({
            cpf: ['', [Validators.required, Validators.minLength(11)]],
            senha: ['', [Validators.required, Validators.minLength(6)]]
        })
    }

    get cpf(): FormControl {
        return <FormControl>this.authForm.get('cpf');
    }

    get senha(): FormControl {
        return <FormControl>this.authForm.get('senha');
    }

    get confirmasenha(): FormControl {
        return <FormControl>this.authForm.get('confirmasenha');
    }

    onSubmit() {
        this.load = true;
        if (this.configs.isSignIn) {
            console.log("logar")
            this.authService.logar(this.authForm.controls['cpf'].value, this.authForm.controls['senha'].value).subscribe(token => {
                console.log("token")
                this.tokenService.setToken(token.Id);
                this.authForm.reset();
                this.load = false;
            }, () =>{
                this.load = false;
                swalAlert('Usuário ou senha Inválidos!', 'error');
            });
        }
        else {
            if (this.authForm.controls['senha'].value !== this.authForm.controls['confirmasenha'].value) {
                swalAlert('As senhas devem ser iguais', 'error');
            }
            else {
                this.accountService.cadastrar(this.authForm.controls['cpf'].value, this.authForm.controls['senha'].value).subscribe(() => {
                    swal.fire('Usuário Cadastrado com sucesso!', '', 'success').then(() => {
                        window.location.reload();
                    }, error => {
                        if (error.status == 409) {
                            swalAlert('Usuário já Cadastrado', 'error');
                        }
                        else {
                            swalAlert('Ocorreu um erro inesperado, aguarde ou tente novamente!', 'error');
                        }
                    })
                })
            }
        }
    }

    changeAuthAction(): void {
        this.configs.isSignIn = !this.configs.isSignIn
        const { isSignIn } = this.configs;
        this.configs.action = isSignIn ? 'Login' : 'Criar Conta';
        this.configs.actionChange = isSignIn ? 'Ainda não tem conta? Criar Conta' : 'Já possui uma conta:Logar';
        !isSignIn ? this.authForm.addControl('confirmasenha', this.confirmaSenhaControl) : this.authForm.removeControl('confirmasenha');
    }
}