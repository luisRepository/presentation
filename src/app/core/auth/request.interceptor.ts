import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from './../token/token.service';
import { environment } from '../../../environments/environment';

const API = environment.Gateway;

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor(private tokenService: TokenService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        if (req.url.startsWith(API)) {
            if (this.tokenService.hasToken()) {
                req = req.clone({
                    setHeaders: { 'TokenId': this.tokenService.getToken(), 'Channel': 'PORTAL' }
                });
            }
            else {
                req = req.clone({
                    setHeaders: { 'Channel': 'PORTAL' }
                })
            }
        }
        return next.handle(req);
    }
}