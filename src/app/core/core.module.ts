import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { HeaderComponent } from "./header/header.component"
import { AlterarSenhaComponent } from './alterar-senha/alterar-senha.component';
import { EnviarTokenComponent } from './alterar-senha/enviar-token/enviar-token.component';
import { ValidarTokenComponent } from './alterar-senha/validar-token/validar-token.component';
import { AuthComponent } from './auth/auth.component';
import { FooterComponent } from './footer/footer.component'
import { SharedModule } from './../shared/shared.module';
import { AccountService } from './services/account.service';
import { SalvarSenhaComponent } from './alterar-senha/salvar-senha/salvar-senha.component';
import { NgxMaskModule } from 'ngx-mask';
import { AuthService } from './services/auth.service';
import { TokenService } from './token/token.service';


@NgModule({
  declarations: [HeaderComponent, FooterComponent, AuthComponent, AlterarSenhaComponent, 
    EnviarTokenComponent, ValidarTokenComponent, SalvarSenhaComponent],
  imports: [

CommonModule,
    RouterModule,
    SharedModule,
    NgxMaskModule.forRoot()
  ],
  providers: [AccountService, AuthService, TokenService],
  exports: [HeaderComponent, FooterComponent, AuthComponent, AlterarSenhaComponent, 
    EnviarTokenComponent, ValidarTokenComponent, SalvarSenhaComponent]
})
export class CoreModule { }