import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';

const apiGateway = environment.Gateway;

@Injectable({
    providedIn: 'root'
})

export class AccountService {

    constructor(private http: HttpClient) { }

    cadastrar(login: string, senha: string): Observable<any> {
        var user = {
            Login: login,
            Password: senha
        };
        return this.http.post<any>(`${apiGateway}/api/user`, user)
    };

    excluirUsuario(login: string): Observable<any> {
        return this.http.delete<any>(`${apiGateway}/api/user?login=${login}`);
    }
}