import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenModel } from './../token/token.model';
import { environment } from './../../../environments/environment';

const ApiGateway = environment.Gateway;

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) { }

    logar(login, senha): Observable<TokenModel> {
        let serializedToken = btoa(`${login}:${senha}`);
        let httpHeaders = new HttpHeaders().set('Authorization', `Basic ${serializedToken}`);
        return this.http.get<TokenModel>(`${ApiGateway}/api/authentication`, { headers: httpHeaders });
    }

    deslogar(): Observable<any>{
        return this.http.delete<any>(`${ApiGateway}/api/authentication`);
    }

    getToken(): Observable<TokenModel>{
        return this.http.get<TokenModel>(`${ApiGateway}/api/authentication/gettoken`);
    }
}