import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ErrorsModule } from './errors/errors.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DadosCadastraisModule } from './dados-cadastrais/dados-cadastrais.module';
import { DadosContatoModule } from './dados-contato/dados-contato.module';
import { RequestInterceptor } from './core/auth/request.interceptor';

@NgModule({
  declarations: [		
    AppComponent
   ],
  imports: [

BrowserModule,
    AppRoutingModule,
    CoreModule,
    ErrorsModule,
    HttpClientModule,
    DadosCadastraisModule,
    DadosContatoModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }