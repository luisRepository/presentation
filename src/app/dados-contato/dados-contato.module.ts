import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { DadosContatoComponent } from './dados-contato.component';

@NgModule({
  declarations: [
    DadosContatoComponent
  ],
  imports: [
    SharedModule
  ],
  providers: [],
  exports: [DadosContatoComponent]
})
export class DadosContatoModule { }