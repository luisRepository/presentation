import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from './../../environments/environment';

const ApiBuscaCep = environment.ApiUrlBuscaCep;

@Component({
    selector: 'app-dados-contato',
    templateUrl: './dados-contato.component.html',
    styleUrls: ['./dados-contato.component.scss']
})
export class DadosContatoComponent implements OnInit {

    public dadosForm: FormGroup;
    public linkBuscaCep: string = ApiBuscaCep;
    public estiloContatoForm: string;

    constructor(private formBuilder: FormBuilder,) { }

    ngOnInit() {
        this.createForm();
    }

    private createForm(): void {
        this.dadosForm = this.formBuilder.group(
            {
                ddiCelular: [''],
                dddCelular: [''],
                celular: ['', [Validators.minLength(8)]],
                dddTelefone: [''],
                telefone: ['', [Validators.maxLength(10)]],
                email: ['', [Validators.email]],
                cep: ['', [Validators.minLength(5), Validators.maxLength(8)]],
                logradouro: ['', [Validators.required]],
                numero: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
                complemento: [''],
                cidade: ['', [Validators.required]],
                bairro: ['', [Validators.required]],
                uf: ['', [Validators.required]],
                pais: ['', [Validators.required]],
                enviarcelular: [false],
                enviaremail: [false]
            }
        )
    }

    contatoAlterado() {

    }

    verificarCelular(valor: any) {

    }

    verificarEmail(valor: any){

    }

    buscarCep(){

    }

    redirecionarFicha(){

    }

    salvar(){

    }

}